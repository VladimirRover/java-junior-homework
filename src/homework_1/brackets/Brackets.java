package homework_1.brackets;

import java.util.ArrayList;

public class Brackets {

    public static boolean isCorrect(String str) {
        int count = 0;

        ArrayList<Character> arrayList = fromStringToList(str);

        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) == '(') {
                if (arrayList.get(i + 1) == ']' || arrayList.get(i + 1) == '}') {
                    return false;
                }
                count++;
            }
            if (arrayList.get(i) == ')') {
                count--;
                if (count < 0) {
                    return false;
                }
            }

            if (arrayList.get(i) == '[') {
                if (arrayList.get(i + 1) == ')' || arrayList.get(i + 1) == '}') {
                    return false;
                }
                count++;
            }
            if (arrayList.get(i) == ']') {
                count--;
                if (count < 0) {
                    return false;
                }
            }

            if (arrayList.get(i) == '{') {
                if (arrayList.get(i + 1) == ')' || arrayList.get(i + 1) == ']') {
                    return false;
                }
                count++;
            }
            if (arrayList.get(i) == '}') {
                count--;
                if (count < 0) {
                    return false;
                }
            }
        }

        if (count != 0) {
            return false;
        }
        return true;
    }

    public static ArrayList<Character> fromStringToList(String str) {
        char[] chars = str.toCharArray();
        ArrayList<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '(' || chars[i] == ')'
                    || chars[i] == '[' || chars[i] == ']'
                    || chars[i] == '{' || chars[i] == '}') {
                arrayList.add(chars[i]);
            }
        }
        return arrayList;
    }

}
