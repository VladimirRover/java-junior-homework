package homework_1.brackets;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BracketsByStack {
    public static void main(String[] args) {
        String input = "";
        System.out.println(isCorrect(input));
    }

    public static boolean isCorrect(String line) {

        if(line.length() == 0){
            return true;
        }
        Stack<Character> stack = new Stack<>();
        List<Character> list = fromStringToList(line);
        int count = 0;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(0) == ')' || list.get(0) == ']' || list.get(0) == '}') {
                return false;
            }
            if (list.get(i) == '(' || list.get(i) == '[' || list.get(i) == '{') {
                stack.push(list.get(i));
                count++;

            }
            if (list.get(i) == ')') {
                count++;
                if (!stack.empty()) {
                    if (stack.pop() == '(') {
                        if (count == list.size()) {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            }
            if (list.get(i) == ']') {
                count++;
                if (!stack.empty()) {
                    if (stack.pop() == '[') {
                        if (count == list.size()) {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            }
            if (list.get(i) == '}') {
                count++;
                if (!stack.empty()) {
                    if (stack.pop() == '{') {
                        if (count == list.size()) {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }

            }
        }

        return false;
    }

    public static ArrayList<Character> fromStringToList(String str) {
        char[] chars = str.toCharArray();
        ArrayList<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '(' || chars[i] == ')'
                    || chars[i] == '[' || chars[i] == ']'
                    || chars[i] == '{' || chars[i] == '}') {
                arrayList.add(chars[i]);
            }
        }
        return arrayList;
    }
}
