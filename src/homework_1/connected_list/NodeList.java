package homework_1.connected_list;

public class NodeList<T> {

    private Node firstNode;
    private static int count = 0;

    public void insert(T n) {
        Node list = new Node();
        list.item = n;
        list.node = firstNode;
        firstNode = list;
        count++;
    }

    public int length() {
        return count;
    }
}