package homework_1.queue_by_two_stacks;

import java.util.Stack;

public class MyQueue<T> {
    private Stack<T> inputStack = new Stack<>();
    private Stack<T> outputStack = new Stack<>();

    public void push(T element) {
        inputStack.push(element);
    }

    public T pop() {
        if (outputStack.empty()){
            turnStack();
        }
        return outputStack.pop();
    }

    private void turnStack(){
        while (!inputStack.empty()) {
            outputStack.push(inputStack.pop());
        }
    }
}