package homework_2.insertsort;

public class InsertionSort {
    public static int[] insertionSort(int[] array) {
        int temp;
        for (int i = 1; i < array.length; i++) {
            temp = array[i];
            int compareIndex = i;
            while (compareIndex > 0 && array[compareIndex - 1] > temp) {
                array[compareIndex] = array[compareIndex - 1];
                compareIndex--;
            }
            array[compareIndex] = temp;
        }
        return array;
    }
}