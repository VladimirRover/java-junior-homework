package homework_1.brackets;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class BracketsByStackTest {
    @Test
    public void test() {

        String correct1 = "({[fff]})";
        String correct2 = "(5435[{fff[fff]}])";
        String incorrect1 = "(({)})";
        String incorrect2 = "[(dd({ddd)}d)]";
        String incorrect3 = "]()";
        String incorrect4 = "()]";
        String incorrect5 = "{]}";
        String incorrect6 = "";
        String incorrect7 = "(((";

        assertTrue(BracketsByStack.isCorrect(correct1));
        assertTrue(BracketsByStack.isCorrect(correct2));
        assertFalse(BracketsByStack.isCorrect(incorrect1));
        assertFalse(BracketsByStack.isCorrect(incorrect2));
        assertFalse(BracketsByStack.isCorrect(incorrect3));
        assertFalse(BracketsByStack.isCorrect(incorrect4));
        assertFalse(BracketsByStack.isCorrect(incorrect5));
        assertTrue(BracketsByStack.isCorrect(incorrect6));
        assertFalse(BracketsByStack.isCorrect(incorrect7));
    }
}
