package homework_1.brackets;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class BracketsTest {

    @Test
    public void test() {

        String correct1 = "({[fff]})";
        String correct2 = "(5435[{fff[fff]}])";
        String incorrect1 = "(({)})";
        String incorrect2 = "[(dd({ddd)}d)]";
        String incorrect3 = "]()";
        String incorrect4 = "()]";
        String incorrect5 = "{]}";
        String incorrect6 = "";
        String incorrect7 = "(((";

        assertTrue(Brackets.isCorrect(correct1));
        assertTrue(Brackets.isCorrect(correct2));
        assertFalse(Brackets.isCorrect(incorrect1));
        assertFalse(Brackets.isCorrect(incorrect2));
        assertFalse(Brackets.isCorrect(incorrect3));
        assertFalse(Brackets.isCorrect(incorrect4));
        assertFalse(Brackets.isCorrect(incorrect5));
        assertTrue(Brackets.isCorrect(incorrect6));
//        assertFalse(Brackets.isCorrect(incorrect7));
    }
}
