package homework_1.connected_list;

import org.junit.Assert;
import org.junit.Test;

public class NodeListTest {

    @Test
    public void test(){
        NodeList nodeList = new NodeList();

        nodeList.insert(1);
        nodeList.insert(2);
        nodeList.insert(3);
        nodeList.insert(4);
        nodeList.insert("5");

        Assert.assertEquals(5, nodeList.length());
    }
}
