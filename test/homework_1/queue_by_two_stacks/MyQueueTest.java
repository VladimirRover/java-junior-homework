package homework_1.queue_by_two_stacks;

import org.junit.Assert;
import org.junit.Test;

import java.util.Stack;

public class MyQueueTest {
    @Test
    public void test(){
        MyQueue myQueue = new MyQueue();

        myQueue.push(1);
        myQueue.push(2);
        myQueue.push(3);

        Assert.assertEquals(1, myQueue.pop());
        Assert.assertEquals(2, myQueue.pop());
        Assert.assertEquals(3, myQueue.pop());
    }
}
