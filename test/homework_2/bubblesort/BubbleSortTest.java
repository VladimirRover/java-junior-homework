package homework_2.bubblesort;

import org.junit.Assert;
import org.junit.Test;

public class BubbleSortTest {
    @Test
    public void test() {
        int[] expectedArray = new int[]{1, 4, 8, 99};
        int[] result = BubbleSort.bubbleSort(new int[]{1, 8, 99, 4});
        Assert.assertArrayEquals(expectedArray, result);
    }
}