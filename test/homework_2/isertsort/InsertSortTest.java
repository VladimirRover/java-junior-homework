package homework_2.isertsort;

import homework_2.insertsort.InsertionSort;
import org.junit.Assert;
import org.junit.Test;

public class InsertSortTest {
    @Test
    public void test() {
        int[] expectedArray = new int[]{1, 4, 8, 99};
        int[] result = InsertionSort.insertionSort(new int[]{1, 8, 99, 4});
        Assert.assertArrayEquals(expectedArray, result);
    }
}