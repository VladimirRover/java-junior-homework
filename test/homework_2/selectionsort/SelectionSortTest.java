package homework_2.selectionsort;

import org.junit.Assert;
import org.junit.Test;

public class SelectionSortTest {
    @Test
    public void test() {
        int[] expectedArray = new int[]{1, 4, 8, 99};
        int[] result = SelectionSort.selectionSort(new int[]{1, 8, 99, 4});
        Assert.assertArrayEquals(expectedArray, result);
    }
}